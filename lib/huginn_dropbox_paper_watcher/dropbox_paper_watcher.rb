require 'net/http'
require 'net/https'
require 'json'

module Agents
  class DropboxPaperWatcher < Agent

    cannot_receive_events!
    default_schedule "every_1m"
    can_dry_run!

    description <<-MD
      The Dropbox Paper Watcher Agent watches for changed documents and emits events for each.
    MD

    event_description <<-MD
      The event payload will contain the following fields:

          {
            "added": [ {
              "doc_id": "abc123"
              "title": "document title",
              "rev": "123",
            } ],
            "removed": [ ... ],
            "updated": [ ... ]
          }
    MD

    def default_options
      {
        'expected_update_period_in_days' => 1
      }
    end

    def validate_options
      errors.add(:base, 'Invalid `expected_update_period_in_days` format.') unless options['expected_update_period_in_days'].present? && is_positive_integer?(options['expected_update_period_in_days'])
    end

    def working?
      event_created_within?(interpolated['expected_update_period_in_days']) && !received_event_without_error?
    end

    def check
      current_docs = list
      diff = DropboxDocDiff.new(previous_docs, current_docs)
      create_event(payload: diff.to_hash) unless previous_docs.nil? || diff.empty?

      remember(current_docs)
    end

    private

    def summarize(doc_id)
      params = {
        :doc_id => doc_id,
        :export_format => "markdown"
      }

      uri = URI('https://api.dropboxapi.com/2/paper/docs/download')
      request = Net::HTTP::Head.new(uri)
      request["Authorization"] = "Bearer #{token}"
      request["Dropbox-API-Arg"] = params.to_json

      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      response = http.request(request)

      results = JSON.parse(response['dropbox-api-result'])

      return {
        :doc_id => doc_id,
        :title => results["title"],
        :rev => results["revision"]
      }
    end

    def list
      params = {
        :filter_by => "docs_created",
        :sort_by => "modified",
        :sort_order => "descending",
        :limit => 100
      }

      uri = URI('https://api.dropboxapi.com/2/paper/docs/list')
      request = Net::HTTP::Post.new(uri, initheader = { 'Content-Type' => 'application/json' })
      request["Authorization"] = "Bearer #{token}"
      request.body = params.to_json

      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      response = http.request(request)
      results = JSON.parse(response.body)

      docs = {}
      results["doc_ids"].each do |doc_id|
        docs[doc_id] = summarize(doc_id)
      end

      return docs
    end

    def previous_docs
      self.memory['docs']
    end

    def remember(docs)
      self.memory['docs'] = docs
    end

    def token
      credential('dropbox')
    end

    # == Auxiliary classes ==

    class DropboxDocDiff
      def initialize(previous, current)
        @previous, @current = [previous || {}, current || {}]
        calculate_diff
      end

      def empty?
        return true if @removed == nil or @updated == nil or @added == nil
        @removed.empty? and @updated.empty? and @added.empty?
      end

      def to_hash
        { added: @added.values, removed: @removed.values, updated: @updated.values }
      end

      private

      def calculate_diff
        @removed = {}
        @added = {}
        @updated = {}

        @current.each do |doc_id, doc|
          previous_entry = @previous[doc_id]
          if !previous_entry
            @added[doc_id] = doc
            next
          end
          next if previous_entry[:rev] == doc[:rev]
          @updated[doc_id] = doc
        end

        @previous.each do |doc_id, doc|
          current_entry = @current[doc_id]
          if !current_entry
            @removed[doc_id] = doc
          end
        end
      end
    end
  end
end
