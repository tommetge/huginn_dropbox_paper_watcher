# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "huginn_dropbox_paper_watcher"
  spec.version       = '0.1'
  spec.authors       = ["tom metge"]
  spec.email         = ["tom@accident-prone.com"]

  spec.summary       = %q{Monitors Dropbox Paper documents for changes.}
  spec.description   = %q{Publishes events detailing additions, removals, and document updates.}

  spec.homepage      = "https://bitbucket.org/tommetge/huginn_dropbox_paper_watcher"

  spec.license       = "MIT"


  spec.files         = Dir['LICENSE.txt', 'lib/**/*']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = Dir['spec/**/*.rb'].reject { |f| f[%r{^spec/huginn}] }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_runtime_dependency "huginn_agent"
end
